#include "hamming.h"
#include "hamming_assert.h"
#include "bits.h"

#include <stddef.h>
#include <malloc.h>

/**
 * @brief Returns the index of the left-most set bit in the number (The number must not be equal to zero).
 */
static uint8_t get_set_msb_index(uint32_t number) {

    ASSERT(number != 0);

    uint8_t current_bit_index = 0;
    uint8_t last_set_bit_index = 0;

    while (number != 0) {
        if (number & 1) {
            last_set_bit_index = current_bit_index;
        }

        current_bit_index++;
        number = number >> 1;
    }

    return last_set_bit_index;
}

/**
 * @brief Encodes the specified data packet and writes it to the given buffer.
 *
 * @param data_bytes The data buffer from which bits should be taken.
 * @param data_bit_offset The offset of the first bit to be encoded.
 * @param n_data_bits The number of bits to encode in the current packet.
 * @param n_bits_per_packet The number of bits that should be present in a single packet (including overhead).
 * @param encoded_bytes The data buffer to which the encoded bits should be written.
 * @param encoded_bit_offset The offset for the first encoded bit in the @code encoded_bytes buffer.
 */
static void encode_data_packet(const uint8_t *data_bytes,
                               uint32_t data_bit_offset,
                               uint32_t n_data_bits,
                               uint32_t n_bits_per_packet,
                               uint8_t *encoded_bytes,
                               uint32_t encoded_bit_offset) {

    // It is assumed that encoded_bytes is sufficiently large.
    const uint32_t MAX_DATA_BITS = n_bits_per_packet - get_set_msb_index(n_bits_per_packet) - 1; // Extended.

    ASSERT(0 < n_data_bits && n_data_bits <= MAX_DATA_BITS);

    uint32_t data_bit_index = 0;
    uint32_t parity_bits = 0;
    uint8_t extension_bit = 0;

    // Fill in data bits.
    for (uint32_t packet_bit_index = 0; packet_bit_index < n_bits_per_packet; packet_bit_index++) {

        if (!is_power_of_two(packet_bit_index) && packet_bit_index != 0) {

            uint8_t data_bit = 0;

            if (data_bit_index < n_data_bits) {
                data_bit = get_array_bit_msb(data_bytes, data_bit_offset + data_bit_index);
            }

            set_array_bit_msb(encoded_bytes, encoded_bit_offset + packet_bit_index, data_bit)

            data_bit_index++;

            if (data_bit) {
                parity_bits ^= packet_bit_index;
            }

            extension_bit ^= data_bit;
        }
    }

    // Fill in parity bits.
    for (uint32_t parity_bit_index = 1;
         parity_bit_index < n_bits_per_packet;
         parity_bit_index = parity_bit_index << 1) {

        uint8_t parity_bit = to_bit(parity_bits & parity_bit_index);
        set_array_bit_msb(encoded_bytes, encoded_bit_offset + parity_bit_index, parity_bit)

        extension_bit ^= parity_bit;
    }

    // Set extended bit.
    set_array_bit_msb(encoded_bytes, encoded_bit_offset, extension_bit)
}


Data *encode_data(const Data *original_data, uint32_t n_bits_per_packet, uint32_t *n_packets) {

    ASSERT(original_data != NULL);
    ASSERT(n_bits_per_packet >= MIN_BITS_PER_PACKET);

    const uint32_t N_DATA_BITS = get_bit_count(original_data);
    const uint32_t N_DATA_BITS_PER_PACKET = n_bits_per_packet - get_set_msb_index(n_bits_per_packet) - 1; // Extended.

    const uint32_t N_PACKETS = (N_DATA_BITS - 1) / N_DATA_BITS_PER_PACKET + 1;
    const uint32_t N_ENCODED_BITS = N_PACKETS * n_bits_per_packet;
    const uint32_t N_ENCODED_BYTES = (N_ENCODED_BITS - 1) / BITS_IN_BYTE + 1;

    uint8_t *encoded_bytes = malloc(sizeof(uint8_t) * N_ENCODED_BYTES);

    if (n_packets != NULL) {
        *n_packets = N_PACKETS;
    }

    const uint8_t *data_bytes = get_bytes(original_data);
    uint32_t data_bit_index = 0;
    uint32_t remaining_data_bits = N_DATA_BITS;
    uint32_t encoded_bit_index = 0;

    for (uint32_t packet_index = 0; packet_index < N_PACKETS; packet_index++) {

        uint32_t n_data_bits =
                remaining_data_bits < N_DATA_BITS_PER_PACKET ? remaining_data_bits : N_DATA_BITS_PER_PACKET;

        encode_data_packet(data_bytes,
                           data_bit_index,
                           n_data_bits,
                           n_bits_per_packet,
                           encoded_bytes,
                           encoded_bit_index);

        data_bit_index += n_data_bits;
        remaining_data_bits -= n_data_bits;
        encoded_bit_index += n_bits_per_packet;
    }

    return data_create_wrap_from(encoded_bytes, 0, N_ENCODED_BITS);
}

/**
 * @brief Decodes the specified data packet and writes the decoded data to the given buffer.
 *
 * @param encoded_bytes The encoded data buffer from which bits should be taken.
 * @param encoded_bit_offset The offset of the first bit to be decoded.
 * @param n_bits_per_packet The number of bits that are present in a single packet (including overhead).
 * @param data_bytes The data buffer to which the decoded bits should be written.
 * @param data_bit_offset The offset for the first decoded bit in the data buffer.
 */
static void decode_data_packet(const uint8_t *encoded_bytes,
                               uint32_t encoded_bit_offset,
                               uint32_t n_bits_per_packet,
                               uint8_t *data_bytes,
                               uint32_t data_bit_offset) {

    uint32_t flipped_bit_index = 0;
    uint32_t data_bit_index = 0;

    // Find index of flipped bit, if any.
    for (uint32_t packet_bit_index = 0; packet_bit_index < n_bits_per_packet; packet_bit_index++) {

        uint8_t data_bit = get_array_bit_msb(encoded_bytes, encoded_bit_offset + packet_bit_index);

        if (data_bit) {
            flipped_bit_index ^= packet_bit_index;
        }
    }

    // Fill in data bits.
    for (uint32_t packet_bit_index = 0; packet_bit_index < n_bits_per_packet; packet_bit_index++) {

        uint8_t data_bit = get_array_bit_msb(encoded_bytes, encoded_bit_offset + packet_bit_index);

        if (!is_power_of_two(packet_bit_index) && packet_bit_index != 0) {

            set_array_bit_msb(data_bytes, data_bit_offset + data_bit_index,
                              data_bit ^ (packet_bit_index == flipped_bit_index))

            data_bit_index++;
        }
    }
}

Data *decode_data(const Data *encoded_data, uint32_t n_bits_per_packet) {

    ASSERT(encoded_data != NULL);
    ASSERT(n_bits_per_packet >= MIN_BITS_PER_PACKET);

    const uint32_t N_ENCODED_BITS = get_bit_count(encoded_data);
    const uint32_t N_PACKETS = N_ENCODED_BITS / n_bits_per_packet;

    ASSERT(N_PACKETS * n_bits_per_packet == N_ENCODED_BITS);

    const uint32_t N_DATA_BITS_PER_PACKET = n_bits_per_packet - get_set_msb_index(n_bits_per_packet) - 1; // Extended.
    const uint32_t N_DATA_BITS = N_DATA_BITS_PER_PACKET * N_PACKETS;
    const uint32_t N_DATA_BYTES = (N_DATA_BITS - 1) / BITS_IN_BYTE + 1;

    uint8_t *data_bytes = malloc(sizeof(uint8_t) * N_DATA_BYTES);

    uint32_t data_bit_index = 0;
    uint32_t encoded_bit_index = 0;
    const uint8_t *encoded_bytes = get_bytes(encoded_data);

    for (uint32_t packet_index = 0; packet_index < N_PACKETS; packet_index++) {

        decode_data_packet(encoded_bytes,
                           encoded_bit_index,
                           n_bits_per_packet,
                           data_bytes,
                           data_bit_index);

        data_bit_index += N_DATA_BITS_PER_PACKET;
        encoded_bit_index += n_bits_per_packet;
    }

    return data_create_wrap_from(data_bytes, 0, N_DATA_BITS);
}