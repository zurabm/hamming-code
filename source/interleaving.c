#include "hamming.h"
#include "hamming_assert.h"
#include "bits.h"

#include <stddef.h>

Data *interleave_data(const Data *data, uint32_t n_groups) {

    ASSERT(data != NULL);
    ASSERT(n_groups > 0);

    const uint32_t N_BITS = get_bit_count(data);
    const uint32_t N_BYTES = get_byte_count(data);
    const uint32_t N_BITS_PER_GROUP = (N_BITS - 1) / n_groups + 1;

    const uint8_t *data_bytes = get_bytes(data);

    uint8_t interleaved_bytes[N_BYTES];
    uint32_t interleaved_bit_index = 0;

    for (uint32_t start_bit_index = 0; start_bit_index < N_BITS_PER_GROUP; start_bit_index++) {
        for (uint32_t data_bit_index = start_bit_index; data_bit_index < N_BITS; data_bit_index += N_BITS_PER_GROUP) {

            uint8_t data_bit = get_array_bit_msb(data_bytes, data_bit_index);
            set_array_bit_msb(interleaved_bytes, interleaved_bit_index, data_bit)

            interleaved_bit_index++;
        }
    }

    Data *interleaved_data = data_create();
    data_initialize(interleaved_data, interleaved_bytes, 0, N_BITS);

    return interleaved_data;
}

Data *revert_data(const Data *data, uint32_t n_groups) {

    ASSERT(data != NULL);
    ASSERT(n_groups > 0);

    const uint32_t N_BITS = get_bit_count(data);
    const uint32_t N_BYTES = get_byte_count(data);
    const uint32_t N_BITS_PER_GROUP = (N_BITS - 1) / n_groups + 1;

    const uint8_t *interleaved_bytes = get_bytes(data);

    uint8_t data_bytes[N_BYTES];
    uint32_t interleaved_bit_index = 0;

    for (uint32_t start_bit_index = 0; start_bit_index < N_BITS_PER_GROUP; start_bit_index++) {
        for (uint32_t data_bit_index = start_bit_index; data_bit_index < N_BITS; data_bit_index += N_BITS_PER_GROUP) {

            uint8_t data_bit = get_array_bit_msb(interleaved_bytes, interleaved_bit_index);
            set_array_bit_msb(data_bytes, data_bit_index, data_bit)

            interleaved_bit_index++;
        }
    }

    Data *original_data = data_create();
    data_initialize(original_data, data_bytes, 0, N_BITS);

    return original_data;
}

