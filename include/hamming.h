#ifndef __HAMMING_H__
#define __HAMMING_H__

#include "data.h"

#define MIN_BITS_PER_PACKET 4

/**
 * @brief Encodes the specified data according to the given encoding parameters.
 *
 * @param original_data The bytes to be encoded as a Data.
 * @param n_bits_per_packet The number of total bits in a single encoded packet.
 * @param n_packets The number of packets the bytes were encoded into will be written to this address (if not null).
 * @return Data* A newly allocated Data structure storing the encoded bytes.
 */
Data *encode_data(const Data *original_data, uint32_t bits_per_packet, uint32_t *n_packets);

/**
 * @brief Decoded the specified data according to the given encoding parameters.
 *
 * @param encoded_data The bytes to be decoded as a Data.
 * @param n_bits_per_packet The number of total bits in a single encoded packet.
 * @return Data* A newly allocated Data structure storing the decoded bytes;
 */
Data *decode_data(const Data *encoded_data, uint32_t n_bits_per_packet);

#endif /* __HAMMING_H__ */