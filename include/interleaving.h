#ifndef __HAMMING_CODE_INTERLEAVING_H__
#define __HAMMING_CODE_INTERLEAVING_H__

#include "data.h"

/**
 * @brief Interleaves the specified data into the given number of groups.
 *
 * @param data A reference to the Data structure containing the data to be interleaved.
 * @param n_groups The number of groups that the data should be divided into.
 * @return Data structure containing the interleaved data.
 */
Data *interleave_data(const Data *data, uint32_t n_groups);

/**
 * @brief Reverts the interleaved data from the given number of groups.
 *
 * @param data A reference to the Data structure containing the data to be interleaved.
 * @param n_groups The number of groups that the data should be divided into.
 * @return Data structure containing the interleaved data.
 */
Data *revert_data(const Data *data, uint32_t n_groups);

#endif /* __HAMMING_CODE_INTERLEAVING_H__ */
