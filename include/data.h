#ifndef __DATA_H__
#define __DATA_H__

#include <inttypes.h>

typedef struct Data Data;


/**
 * @brief Allocates a new Data structure.
 *
 * @return Data * A pointer to the newly allocated Data structure.
 */
Data *data_create();

/**
 * @brief Allocates and initializes a new Data structure from the specified data.
 *
 * @param data_bytes The bytes to be stored in the data (copied to heap).
 * @param n_bytes The size of the data (in bytes).
 * @param n_bits The size of the data (in bits).
 *
 * @return Data * A pointer to the newly allocated and initialized Data structure.
 *
 * @note The specified array of bytes is copied to a new location in the heap.
 * @note Either @code n_bytes @endcode or @code n_bits @endcode
 *       should be specified (the other should be specified as zero).
 */
Data *data_create_from(const uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits);

/**
 * @brief Allocates and initializes a new Data structure from the specified data.
 *
 * @param data_bytes The bytes to be stored in the data (copied to heap).
 * @param n_bytes The size of the data (in bytes).
 * @param n_bits The size of the data (in bits).
 *
 * @return Data * A pointer to the newly allocated and initialized Data structure, wrapping the specified array of
 *         bytes.
 *
 * @note The specified array of bytes is not copied.
 * @note Either @code n_bytes @endcode or @code n_bits @endcode
 *       should be specified (the other should be specified as zero).
 */
Data *data_create_wrap_from(uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits);

/**
 * @brief Initializes the given Data structure with the specified data bytes.
 *
 * @param data The Data structure to be initialized.
 * @param data_bytes The bytes to be stored in the data (copied to heap).
 * @param n_bytes The size of the data (in bytes).
 * @param n_bits The size of the data (in bits).
 *
 * @note The specified array of bytes is copied to a new location in the heap.
 * @note Either @code n_bytes @endcode or @code n_bits @endcode
 *       should be specified (the other should be specified as zero).
 */
void data_initialize(Data *data, const uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits);

/**
 * @brief Initializes the given Data structure with the specified data bytes.
 *
 * @param data The Data structure to be initialized.
 * @param data_bytes The bytes to be stored in the data (copied to heap).
 * @param n_bytes The size of the data (in bytes).
 * @param n_bits The size of the data (in bits).
 *
 * @note The specified array of bytes is not copied.
 * @note Either @code n_bytes @endcode or @code n_bits @endcode
 *       should be specified (the other should be specified as zero).
 */
void data_wrap(Data *data, uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits);

/**
 * @brief Get the data bytes stored in the specified Data.
 * 
 * @param data The Data structure.
 * @return uint8_t* The bytes as a pointer to a character array.
 */
const uint8_t *get_bytes(const Data *data);

/**
 * @brief Get the number of bytes stored in the specified Data/
 * 
 * @param data The Data structure.
 * @return uint32_t The number of bytes.
 */
uint32_t get_byte_count(const Data *data);

/**
 * @brief Get the number of bits stored in the specified Data.
 *
 * @param data The Data structure.
 * @return uint32_t The number of bits.
 */
uint32_t get_bit_count(const Data *data);

/**
 * @brief Deallocates memory used by the specified Data structure.
 *
 * @param data A reference to the Data to be disposed.
 */
void data_dispose(Data *data);

/**
 * @brief Deallocates memory used by the specified Data structure, without deallocating the memory used by
 * the underlying byte array.
 *
 * @param data A reference to the Data to be disposed.
 */
void data_dispose_wrap(Data *data);


#endif /* __DATA_H__ */