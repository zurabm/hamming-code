# Hamming Code

This is a personal project implementing Extended Hamming Encoding in C, meant to be used with embedded devices to
send/receive packets of data using Error-Correcting Code (ECC).